(ns database.core
  (:require [clojure.java.jdbc :as sql]))

(def db {:classname "com.mysql.jdbc.Driver"
              :subprotocol "mysql"
              :subname "//localhost:3306/abcdb"
              :user "root"
              :password "Mysql12345"})


(defn show-abc
  []
  (println (sql/query db ["select * from a"])))


(defn app
  []
  (show-abc))